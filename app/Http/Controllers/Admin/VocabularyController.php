<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\VocabularyDataTable;
use App\Http\Controllers\Controller;
use App\Models\Vocabulary;
use Illuminate\Http\Request;
use PHPUnit\Exception;

class VocabularyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param VocabularyDataTable $dataTable
     * @return void
     */
    public function index(VocabularyDataTable $dataTable)
    {
        return $dataTable->render('admin.vocabulary.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Vocabulary $vocabulary
     * @return void
     */
    public function destroy(Vocabulary $vocabulary)
    {
        try {
            $result = $vocabulary->delete();
            return response()->json(['data' => ['message' => 'Xoá thành công', 'status' => 'success']], 200);
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }
}
