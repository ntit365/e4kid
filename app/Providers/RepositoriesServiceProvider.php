<?php

namespace App\Providers;

use App\Models\Vocabulary;
use App\Repositories\Eloquent\EloquentVocabularyRepository;
use App\Repositories\VocabularyRepository;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(VocabularyRepository::class, function () {
            return new EloquentVocabularyRepository(new Vocabulary());
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
