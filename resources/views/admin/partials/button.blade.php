@if(isset($model) && isset($actions))
    @foreach($actions as $key => $action)
        <a class="btn {{ $action['class'] }}" data-id="{{ $model->id }}" href="{{ isset($action['url']) ? $action['url'] : 'javascript:void(0);' }}">{{ $action['title'] }}</a>
    @endforeach
@endif
