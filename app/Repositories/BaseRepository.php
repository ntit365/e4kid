<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface CoreRepository
 * @package Modules\Core\Repositories
 */
interface BaseRepository
{
    /**
     * @param int $id
     * @return Model $model
     */
    public function find($id);

    /**
     * Return a collection of all elements of the resource
     * @return Collection
     */
    public function all();

    /**
     * @return Builder
     */
    public function allWithBuilder(): Builder;

    /**
     * Paginate the model to $perPage items per page
     * @param int $perPage
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function paginate($perPage = 15);

    /**
     * Create a resource
     * @param  $data
     * @return $model
     */
    public function create($data);

    /**
     * Update a resource
     * @param  $model
     * @param array $data
     * @return Model $model
     */
    public function update($model, $data);

    /**
     * Delete a resource
     * @param  $model
     * @return bool
     */
    public function delete($model);

    /**
     * Restore a resource
     * @param $model
     * @return mixed
     */
    public function restore($model);

    /**
     * Return resources translated in the given language
     * @param string $lang
     * @return Collection
     */
    public function allTranslatedIn($lang);

    /**
     * Find a resource by the given slug
     * @param string $slug
     * @return $model
     */
    public function findBySlug($slug);

    /**
     * Find a resource by an array of attributes
     * @param array $attributes
     * @return $model
     */
    public function findByAttributes(array $attributes);

    /**
     * Return a collection of elements who's ids match
     * @param array $ids
     * @return Collection
     */
    public function findByMany(array $ids);

    /**
     * Get resources by an array of attributes
     * @param array $attributes
     * @param null|string $orderBy
     * @param string $sortOrder
     * @return Collection
     */
    public function getByAttributes(array $attributes, $orderBy = null, $sortOrder = 'asc');
}
