<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\Auditable\AuditableWithDeletesTrait;

class Vocabulary extends Model
{
    use SoftDeletes, AuditableWithDeletesTrait;

    protected $table = 'vocabularies';

    protected $fillable = [
        'status',
        'title',
        'transcription',
        'example',
        'thumbnail',
        'audio',
        'video'
    ];
}
