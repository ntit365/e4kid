@extends('base')

@section('title')
    Login
@endsection

@section('body')
    <div class="blur-bg-images"></div>
    <div class="auth-wrapper">
        <div class="auth-content container">
            <div class="card">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="card-body">
                            <h2 class="mb-4">Welcome to <span class="text-c-blue">Larashop</span></h2>

                            {!! Form::open(['method' => 'POST', 'route' => 'admin.login']) !!}
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="feather icon-mail"></i></span>
                                    </div>
                                    {!! Form::email('email',old('email'),[
                                        'class' => 'form-control'.  ($errors->has('email') ? ' is-invalid' : ''),
                                        'placeholder' => 'Email'
                                    ]) !!}
                                    {!! Form::error ('email') !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="feather icon-lock"></i></span>
                                    </div>
                                    {!! Form::password('password',[
                                        'class' => 'form-control'.  ($errors->has('password') ? ' is-invalid' : ''),
                                        'placeholder' => 'Password'
                                    ]) !!}
                                    {!! Form::error('password') !!}
                                </div>
                            </div>
                            <div class="form-group text-left">
                                <div class="checkbox checkbox-primary d-inline">
                                    {!! Form::checkbox('remember', null, false, ['id' => 'remember']) !!}
                                    {!! Form::label('remember', 'Save credentials', ['class' => 'cr']) !!}
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary mb-4">Login</button>
                            {!! Form::close() !!}
                            <p class="mb-2 text-muted">Forgot password? <a href="auth-reset-password.html"
                                                                           class="f-w-400">Reset</a></p>
                        </div>
                    </div>
                    <div class="col-md-6 d-none d-md-block">
                        <img src="{{ asset('assets/admin/images/auth/img-auth-2.jpg') }}" alt=""
                             class="img-fluid bd-placeholder-img bd-placeholder-img-lg d-block w-100">
                        <img src="{{ asset('assets/admin/images/logo.png') }}" alt=""
                             class="img-fluid img-logo-overlay">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    @foreach(config('assets.admin.required.css') as $asset)
        <link rel="stylesheet" href="{{ asset($asset) }}" type="text/css">
    @endforeach
@endpush

@push('scripts')
    @foreach(config('assets.admin.required.js') as $asset)
        <script src="{{ asset($asset) }}" type="text/javascript"></script>
    @endforeach
    <script src="{{ asset('assets/admin/js/main.js') }}" type="module"></script>
@endpush

