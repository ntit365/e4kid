@if (session()->has('status'))
    <script>
        $(function () {
            let message = '{{ session()->get('message') }}';
            let type = '{{ session()->get('status') }}';
            let title = '{{ session()->get('title') }}';
            show_notify(type, message, title);
        });
    </script>
@endif

{!! session()->forget('status'); !!}
