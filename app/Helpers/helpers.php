<?php

if (!function_exists('removeEmptyArrayElement')) {
    function removeEmptyArrayElement($attribute)
    {
        if ($attribute === 0 || $attribute === false) {
            return true;
        }
        return !empty($attribute);
    }
}


if (!function_exists('controller_name')) {
    function controller_name()
    {
        $routeArray = Str::parseCallback(\Illuminate\Support\Facades\Route::currentRouteAction(), null);
        if (last($routeArray) != null) {
            $controller = str_replace('Controller', '', class_basename(head($routeArray)));
            return strtolower($controller);
        }
        return 'closure';

    }
}
