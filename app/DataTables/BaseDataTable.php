<?php

namespace App\DataTables;

use App\DataTables\Builders\BaseBuilder;
use App\DataTables\Scopes\BaseScope;
use Yajra\DataTables\Services\DataTable;

class BaseDataTable extends DataTable
{

    protected $resource_url;

    public function __construct()
    {
        $this->addScope(new BaseScope($this->getFilters()));
    }


    public function renderAjaxAndActions()
    {
        if ($this->request()->ajax() && $this->request()->wantsJson()) {
            return app()->call([$this, 'ajax']);
        }

        if ($action = $this->request->get('action') and in_array($action, $this->actions)) {
            if ($action == 'print') {
                return app()->call([$this, 'printPreview']);
            }
            return app()->call([$this, $action]);
        }
    }

    /**
     * @param $resource_url
     * @return $this
     */
    public function setResourceUrl($resource_url)
    {
        $this->resource_url = $resource_url;
        return $this;
    }

    public function builder()
    {
        return app(BaseBuilder::class);
    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->minifiedAjax(
                $this->resource_url,
                '$.each(filters("#' . $this->getTableId() . '"), function(name,value){
                        data[name] = value;
                });'
            )
            ->setFilters($this->getFilters())
            ->setOptions($this->getOptions())
            ->setTableId($this->getTableId())
            ->columns($this->getColumns())
            ->addAction(['width' => '10%'])
            ->parameters([
                //'language'   => ['url' => url('datatable/i18n')],
                'order'      => [[0, 'desc']],
                'pageLength' => 10,
            ], $this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [];
    }

    protected function getFilters()
    {
        return [];
    }

    protected function getOptions()
    {
        return [];
    }

    protected function getBuilderParameters()
    {
        return [];
    }

    protected function getTableId()
    {
        return class_basename($this);
    }
}
