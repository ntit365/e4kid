<?php

Route::get('/', 'DashboardController@index');

Route::prefix('auth')->group(function () {
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('login', 'Auth\LoginController@login');
    Route::get('logout', 'Auth\LoginController@logout')->name('admin.logout');
});

Route::resource('vocabulary', 'VocabularyController');
