<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $questions = Question::all();
        $data = [
            'type' => 'success',
            'data' => $questions
        ];
        return response()->json($data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $question = Question::create($data);
        $data = [
            'type' => 'success',
            'data' => 'Tạo câu hỏi thành công'
        ];
        return response()->json($data, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $quesstion = Question::findOrFail($id);
        $data = [
            'type' => 'success',
            'data' => $quesstion
        ];
        return response()->json($data, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $question = Question::findOrFail($id);
        $question->update($data);
        $data = [
            'type' => 'success',
            'data' => 'Cập nhật câu hỏi thành công'
        ];
        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $question = Question::findOrFail($id);
        $question->delete();
        $data = [
            'type' => 'success',
            'data' => 'Xoá câu hỏi thành công'
        ];
        return response()->json($data, 200);
    }
}
