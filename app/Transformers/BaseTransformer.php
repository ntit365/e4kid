<?php

namespace App\Transformers;

use App\Base;
use League\Fractal\TransformerAbstract;

class BaseTransformer extends TransformerAbstract
{
    protected function actions($model, $actions = [], $merge_actions = true)
    {
        if ($merge_actions) {
            $actions = array_merge([
                'edit' => [
                    'title' => trans('general.btn_edit'),
                    'class' => 'btn-primary btn-edit'
                ],
                'delete' => [
                    'title' => trans('general.btn_delete'),
                    'class' => 'btn-danger btn-delete',
                    'url' => route(controller_name().'.destroy', $model)
                ]
            ], $actions);
        }

        $actions = array_filter($actions, 'removeEmptyArrayElement');

        if (view()->exists('admin.partials.button')) {
            try {
                return view('admin.partials.button', ['model' => $model, 'actions' => $actions])->render();
            } catch (\Throwable $e) {
            }
        } else {
            return '';
        }
    }
}
