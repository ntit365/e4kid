$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

const confirm_alert = Swal.mixin({
    customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
});

let INIT = {
    notice_option: {
        "closeButton": true,
        "debug": false,
        "progressBar": true,
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    },
    create_slug: function (source, target) {
        let title = $("input[name=" + source + "]").val();
        let slug = title.toLowerCase();
        slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
        slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
        slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
        slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
        slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
        slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
        slug = slug.replace(/đ/gi, 'd');
        slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
        slug = slug.replace(/ /gi, " - ");
        slug = slug.replace(/\-\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-/gi, '-');
        slug = slug.replace(/\-\-/gi, '-');
        slug = '@' + slug + '@';
        slug = slug.replace(/\@\-|\-\@|\@/gi, '');
        $("input[name=" + target + "]").val(slug);
    },
    ajax_request: function (type, url, data_type, data_request, before_cb = null, success_cb = null) {
        $.ajax({
            url: url,
            type: type,
            data: data_request,
            dataType: data_type,
            beforeSend: before_cb,
            success: success_cb,
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    }
};

let init_slug = (source, target) => {
    $("input[name=" + source + "]").on('input', function () {
        INIT.create_slug(source, target);
    });
};

let format_number = (number, decimals, dec_point, thousands_sep) => {
    let n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === "undefined") ? ',' : thousands_sep,
        dec = (typeof dec_point === "undefined") ? '.' : dec_point,
        toFixedFix = function (n, prec) {
            let k = Math.pow(10, prec);
            return Math.round(n * k) / k;
        },
        s = (prec ? toFixedFix(n, prec) : Math.round(n)).toString().split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length > prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
};

let show_notify = (messageType, message, title) => {
    toastr.clear();
    toastr.options = INIT.notice_option;
    toastr[messageType](message, title);
};

let show_confirm = (options, confirm_cb = null, cancel_cb = null) => {
    confirm_alert.fire({
        title: !(options.title === '') ? options.title : 'Are you sure?',
        text: !(options.text === '') ? options.text : "You won't be able to revert this!",
        type: !(options.type === '') ? options.type : 'warning',
        showCancelButton: true,
        confirmButtonText: !(options.btnConfirmText === '') ? options.btnConfirmText : 'Yes',
        cancelButtonText: !(options.btnCancelText === '') ? options.btnCancelText : 'No',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            return confirm_cb();
        } else {
            return cancel_cb();
        }
    })
};

let filters = (table_id) => {
    let filter = $(table_id + "_filters" + ' .filter');
    let serialized = filter.serialize();
    return {'filters': serialized};
};

let refresh_table = () => {
    window.LaravelDataTables[window.tableId].draw();
};

let redirect_to = (data) => {
    setTimeout(function () {
        window.location.replace(data.url);
    }, 1000);
};

let site_reload = () => {
    setTimeout(function () {
        location.reload();
    }, 1000)
};

let clear_form = (form) => {
    form[0].reset();
};

let delete_item = (url) => {
    let confirm_cb = () => {
        let success_cb = (response) => {
            console.log(response);
            show_notify(response.data.message, response.data.status);
            refresh_table();
        };
        http.delete(url, "JSON", '', '', success_cb);
    };
    let options = {
        title: 'Thông báo',
        text: 'Bạn có chắc chắn muốn xoá?',
        type: 'warning',
        btnConfirmText: 'Có',
        btnCancelText: 'Không'
    };
    show_confirm(options, confirm_cb);
};


let get_url_params = () => {
    let search_params = window.location.search.substr(1);

    let params = {};

    if (search_params !== null && search_params !== "") {

        let params_array = search_params.split("&");

        for (let i = 0; i < params_array.length; i++) {
            let parameter = params_array[i].split("=");
            params[parameter[0]] = parameter[1];
        }
    }

    return params;
};

let http = {
    post: (url, data_type, data_request, before_cb = null, success_cb = null) => {
        return INIT.ajax_request("POST", url, data_type, data_request, before_cb, success_cb);
    },
    get: (url, data_type, data_request, before_cb = null, success_cb = null) => {
        return INIT.ajax_request("POST", url, data_type, data_request, before_cb, success_cb);
    },
    put: (url, data_type, data_request, before_cb = null, success_cb = null) => {
        return INIT.ajax_request("PUT", url, data_type, data_request, before_cb, success_cb);
    },
    patch: (url, data_type, data_request, before_cb = null, success_cb = null) => {
        return INIT.ajax_request("PATCH", url, data_type, data_request, before_cb, success_cb);
    },
    delete: (url, data_type, data_request, before_cb = null, success_cb = null) => {
        return INIT.ajax_request("DELETE", url, data_type, data_request, before_cb, success_cb);
    }
};

$(document).ready(function () {
    $('body').on('click', '.btn-delete', function (e) {
        e.preventDefault();
        let url = $(this).attr('href');
        delete_item(url);
    });
});
