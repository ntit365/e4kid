@extends('admin.layouts.app')
@section('content')
    <div class="row">

        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>List Product</h5>
{{--                   <a class="btn btn-success float-right"  href="{{ route('product.create') }}">{{ trans('product.btn.create') }}</a>--}}
                </div>
                <div class="card-body">
                    @if(!empty($dataTable->filters()))
                        {!! $dataTable->filters() !!}
                    @endif
                    <div class="table-responsive">
                        {!! $dataTable->table(['class' => 'table table-striped table-bordered nowrap', 'width' => '100%']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script type="text/javascript">
        window.tableId = '{{ $dataTable->getTableAttribute('id') }}';
    </script>
    {!! $dataTable->scripts() !!}
@endsection
