<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" id="modal_form" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title h4"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form id="form">
                    @if(view()->exists('admin.'.controller_name().'.partials.fields'))
                        @include('admin.'.controller_name().'.partials.fields')
                    @endif
                </form>
            </div>
            <div class="modal-footer float-right">
                <button class="btn btn-primary btn-save">{{ trans('general.btn_save') }}</button>
            </div>
        </div>
    </div>
</div>

