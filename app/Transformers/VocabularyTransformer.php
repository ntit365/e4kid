<?php

namespace App\Transformers;

use App\Models\Vocabulary;
use League\Fractal\TransformerAbstract;

class VocabularyTransformer extends BaseTransformer
{
    /**
     * @param Vocabulary $vocabulary
     * @return array
     */
    public function transform(Vocabulary $vocabulary)
    {
        return [
            'id' => (int) $vocabulary->id,
            'title' => $vocabulary->title,
            'action' => $this->actions($vocabulary)
        ];
    }
}
