<?php

namespace App\Console\Commands\Installers\Scripts;

use Illuminate\Console\Command;
use App\Console\Commands\Installers\SetupScript;
use App\Console\Commands\Installers\Writers\EnvFileWriter;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class CreateEnvFile implements SetupScript
{
    /**
     * @var EnvFileWriter
     */
    protected $env;
    /**
     * @param EnvFileWriter $env
     */
    public function __construct(EnvFileWriter $env)
    {
        $this->env = $env;
    }
    /**
     * @var Command
     */
    protected $command;

    /**
     * Fire the install script
     * @param Command $command
     * @return mixed
     * @throws FileNotFoundException
     */
    public function fire(Command $command)
    {
        $this->command = $command;
        $this->env->create();
        $command->info('Successfully created .env file');
    }
}
