<?php

namespace App\Console\Commands\Installers\Scripts;

use App\Console\Commands\Installers\SetupScript;
use App\Console\Commands\Installers\Writers\EnvFileWriter;
use Illuminate\Contracts\Config\Repository as Config;
use Illuminate\Console\Command;

class ConfigureAppUrl implements SetupScript
{
    /**
     * @var Config $config
     */
    protected $config;

    /**
     * @var EnvFileWriter $env
     */
    protected $env;

    /**
     * @var Command $command
     */
    protected $command;

    public function __construct(Config $config, EnvFileWriter $env)
    {
        $this->config = $config;
        $this->env = $env;
    }

    public function fire(Command $command)
    {
        $this->command = $command;

        $vars = [];

        $vars['app_url'] = $this->askAppUrl();

        $this->setLaravelConfiguration($vars);

        $this->env->write($vars);

        if ($command->option('verbose')) {
            $command->info('Application url successfully configured');
        }
    }

    private function askAppUrl()
    {
        do {
            $message = 'Enter your application url (e.g. http://localhost, http://dev.example.com)';
            $str = $this->command->ask($message, 'http://localhost');

            if ($str == '' || (strpos($str, 'http://') !== 0 && strpos($str, 'https://') !== 0)) {
                $this->command->error('A valid http:// or https:// url is required');

                $str = false;
            }
        } while (!$str);

        return $str;
    }

    protected function setLaravelConfiguration($vars)
    {
        $this->config['app.url'] = $vars['app_url'];
    }
}
