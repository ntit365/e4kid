@extends('base')
@section('title')
@endsection
@section('body')
    <div class="loader-bg">
        <div class="loader-track">
            <div class="loader-fill"></div>
        </div>
    </div>

    @include('admin.partials.sidebar')
    @include('admin.partials.navbar')


    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    @foreach(config('assets.admin.required.css') as $asset)
        <link rel="stylesheet" href="{{ asset($asset) }}" type="text/css">
    @endforeach
@endpush
@push('scripts')
    @foreach(config('assets.admin.required.js') as $asset)
        <script src="{{ asset($asset) }}" type="text/javascript"></script>
    @endforeach
@endpush



