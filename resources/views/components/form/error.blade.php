@if ($errors->has($name))
    <p class="invalid-feedback">{{ $errors->first($name) }}</p>
@endif
