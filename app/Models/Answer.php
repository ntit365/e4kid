<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'answers';

    protected $fillable = [
        'title',
        'id_question',
        'status',
        'is_true'
    ];

    protected $hidden = ['is_true'];

    public function question()
    {
        $this->belongsTo(Question::class, 'id', 'id_question');
    }
}
