<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>@yield('title')</title>
    <link rel="icon" href="http://html.phoenixcoded.net/elite-able/bootstrap/assets/images/favicon.ico" type="image/x-icon">

    @stack('meta')
    @stack('styles')
    @yield('page-styles')
</head>
<body @yield('body-attributes')>
@yield('body')
@stack('scripts')
@yield('page-scripts')
</body>
</html>
