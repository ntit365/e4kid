<?php
return [
    'action' => 'Action',
    'select' => 'Select',
    'btn_edit' => 'Edit',
    'btn_delete' => 'Delete',
    'btn_save' => 'Save',
];
