<?php

namespace App\Console\Commands;

use App\Console\Commands\Installers\Installer;
use App\Console\Commands\Installers\Scripts\ConfigureAppUrl;
use App\Console\Commands\Installers\Scripts\CreateEnvFile;
use App\Console\Commands\Installers\Scripts\ProtectInstaller;
use App\Console\Commands\Installers\Writers\ConfigureDatabase;
use Illuminate\Console\Command;

class SetupDemo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setup:demo {--f|force : Force the installation, even if already installed}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup development environment';

    /**
     * @var Installer $installer
     */
    private $installer;

    /**
     * Create a new command instance.
     *
     * @param Installer $installer
     */
    public function __construct(Installer $installer)
    {
        parent::__construct();
        $this->getLaravel()['env'] = 'local';
        $this->installer = $installer;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $success = $this->installer->stack([
            ProtectInstaller::class,
            CreateEnvFile::class,
            ConfigureDatabase::class,
            ConfigureAppUrl::class
        ])->install($this);

        if ($success) {
            if (!config('app.key') && file_exists(base_path('.env'))) {
                $this->call('key:generate');
            }

            if (!file_exists(public_path('storage'))) {
                $this->call('storage:link');
            }

            $this->call('optimize:clear');
            $this->call('migrate:fresh', ['--force' => true]);

            $this->call('db:seed');
        }
    }
}
