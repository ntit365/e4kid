<?php

namespace App\DataTables;

use App\Repositories\VocabularyRepository;
use App\Transformers\VocabularyTransformer;

class VocabularyDataTable extends BaseDataTable
{
    public function dataTable(VocabularyRepository $vocabulary)
    {
        $this->setResourceUrl(controller_name());
        return datatables()->collection($vocabulary->all())->setTransformer(new VocabularyTransformer());
    }

    protected function getColumns()
    {
        return [
            'id' => ['title' => 'ID'],
            'title' => ['title' => 'title'],
        ];
    }
}
