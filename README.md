# HỆ THỐNG HỌC TIẾNG ANH CHO TRẺ
**Version 1. 0**



## Stack

  

* Laravel 6.0 and PHP 7.2.1+

* MySQL 5.7+


## Feature

* Command setup demo for developer

  

## Development

  

### Install and run development environment

  

```bash

$ git clone git@gitlab.com:ntit365/e4kid.git /path/to/proj_dir

$ cd /path/to/proj_dir

$ git checkout dev && git pull

$ composer install --ignore-platform-reqs --no-interaction --no-progress --no-scripts --prefer-dist

$ php artisan setup:demo

```

### User admin

```bash

Email: admin@e4kid.com
Password: 12345678

```

### Run check coding style and unit test before commit

```bash

$ ./vendor/bin/phpcs

$ ./vendor/bin/phpunit

```
