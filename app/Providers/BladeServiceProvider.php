<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Collective\Html\FormFacade as Form;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //Custom error form
        Form::component('error', 'components.form.error', ['name']);

        //Custom help form
        Form::component('help', 'components.form.help', ['content']);

        //Custom plan text form
        Form::component('plan_text', 'components.form.plan_text', ['name', 'value']);
    }
}
