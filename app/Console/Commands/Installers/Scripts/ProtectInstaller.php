<?php

namespace App\Console\Commands\Installers\Scripts;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use App\Console\Commands\Installers\SetupScript;

class ProtectInstaller implements SetupScript
{
    /**
     * @var Filesystem
     */
    protected $finder;

    /**
     * @param Filesystem $finder
     */
    public function __construct(Filesystem $finder)
    {
        $this->finder = $finder;
    }

    /**
     * Fire the install script
     * @param Command $command
     * @return mixed
     * @throws Exception
     */
    public function fire(Command $command)
    {
        if ($this->finder->isFile('.env') && !$command->option('force')) {
            $message = 'Your app has already been installed. You can already log into your administration.';
            $message .= ' Run \'php artisan setup:demo -f\' to force replace the .env file.';
            throw new Exception($message);
        }
    }
}
