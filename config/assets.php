<?php
return [
    'admin' => [
        'required' => [
            'css' => [
                'fontawesome' => 'assets/admin/fonts/font-awesome-4.7.0/css/font-awesome.min.css',
                'animate'     => 'assets/admin/plugins/animation/css/animate.min.css',
                'style'       => 'assets/admin/css/style.css',
                'data-table'  => 'assets/admin/plugins/DataTables/datatables.min.css',
                'toastr'      => 'assets/admin/plugins/toastr-2.1.4/toastr.min.css',
                'sweet-alert' => 'assets/admin/plugins/sweetalert/sweetalert2.min.css',
            ],
            'js'  => [
                'vendor'      => 'assets/admin/js/vendor-all.min.js',
                'bootstrap'   => 'assets/admin/plugins/bootstrap/js/bootstrap.min.js',
                'pcoded'      => 'assets/admin/js/pcoded.min.js',
                'data-table'  => 'assets/admin/plugins/DataTables/datatables.min.js',
                'toastr'      => 'assets/admin/plugins/toastr-2.1.4/toastr.min.js',
                'sweet-alert' => 'assets/admin/plugins/sweetalert/sweetalert2.all.min.js',
                'main'        => 'assets/admin/js/main.js',
            ],
        ],
    ],
];
