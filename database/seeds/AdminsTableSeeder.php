<?php

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            'email' => 'admin@e4kid.com',
            'name'  => 'Admin',
            'password' => Hash::make('12345678')
        ];
        Admin::create($user);
    }
}
