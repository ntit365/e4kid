<?php

namespace App\DataTables\Builders;

use Collective\Html\FormFacade as Form;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Html\Column;

class BaseBuilder extends Builder
{
    public $filters;
    public $options;
    public $resource_url;

    public function setFilters($filters = [])
    {
        $this->filters = $filters;
        return $this;
    }

    public function setOptions($options = [])
    {
        $this->options = $options;
        return $this;
    }

    public function setTableId($id)
    {
        return parent::setTableId($id);
    }

    public function filters()
    {
        $filtersFields = $this->filters;
        $tableId = $this->getTableAttribute('id');
        $filters = '<div class="filters p-b-20" data-table="' . $tableId . '" id="' . $tableId . '_filters">';
        $rowColumns = 0;
        foreach ($filtersFields as $key => $field) {
            if (!$field['active']) {
                continue;
            }
            $classArray = explode('-', $field['class']);
            $colNumber = $classArray[count($classArray) - 1];
            if ($rowColumns == 0) {
                $filters .= '<div class="row" >';
            }
            if ($rowColumns > 0 && ($rowColumns + $colNumber) > 12) {
                $rowColumns = 0;
                //row closing
                $filters .= '</div>';
                //start new row
                $filters .= '<div class="row" >';
            }
            $filters .= '<div class="' . $field['class'] . '">';
            $attributes = ['class' => 'filter form-control', 'placeholder' => $field['placeholder'] ?? $field['title']];
            switch ($field['type']) {
                case 'text':
                    $filters .= Form::text($key, null, $attributes);
                    break;
                case 'number':
                    $filters .= Form::number($key, null, false, null, $attributes);
                    break;
                case 'date':
                    $filters .= Form::date($key, null, false, null, $attributes);
                    break;
                case 'date_range':
                    $filters .= Form::dateRange($key, '', false, null, $attributes);
                    break;
                case 'select':
                    $filters .= Form::select($key, null, $field['options'], false, null, [
                        'placeholder' => $placeholder = trans('general.select', ['label' => $field['title']]),
                        'class'       => 'filter'
                    ]);
                    break;
                case 'select2':
                    $filters .= Form::select($key, null, $field['options'], false, null, [
                        'data-placeholder' => trans('general.select', ['label' => $field['title']]),
                        'class'            => 'filter'
                    ]);
                    break;
                case 'boolean':
                    $filters .= Form::checkbox($key, $field['title'], false, $field['checked_value'] ?? 1, [
                        'class' => 'filter'
                    ]);
                    break;
            }
            //col closing
            $filters .= '</div>';
            if (is_numeric($colNumber)) {
                if (($rowColumns + $colNumber) >= 12) {
                    $rowColumns = 0;
                    //row closing
                    $filters .= '</div>';
                } else {
                    $rowColumns += $colNumber;
                }
            }
        }
        if ($rowColumns + 1 > 12) {
            //row closing
            $filters .= '</div>';
            $rowColumns = 0;
        }
        if ($rowColumns == 0) {
            $filters .= '<div class="row" >';
        }
        if (!empty($filtersFields)) {
            $filters .= '<div class="col-md-2 p-r-0 text-left">' .
                Form::button('<i class="fa fa-search m-r-15"></i>', [
                    'class'      => 'btn btn-primary filterBtn',
                    'data-table' => $tableId
                ]) .
                Form::button('<i class="fa fa-eraser"></i>', [
                    'class'      => 'btn btn-danger clearBtn',
                    'data-table' => $tableId
                ]);
            $filters .= '</div></div></div>';
        } else {
            $filters = '';
        }
        return $filters;
    }

    public function addAction(array $attributes = [], $prepend = false)
    {
        $options = $this->options;
        if (isset($options['has_action']) && !$options['has_action']) {
            return $this;
        }
        $attributes = array_merge([
            'defaultContent' => '',
            'data'           => 'action',
            'name'           => 'action',
            'title'          => trans('general.action'),
            'render'         => null,
            'orderable'      => false,
            'searchable'     => false,
            'exportable'     => false,
            'printable'      => true,
            'footer'         => '',
        ], $attributes);
        $this->collection->push(new Column($attributes));
        return $this;
    }

    public function scripts($script = null, array $attributes = ['type' => 'text/javascript'])
    {
        $tableId = $this->getTableAttribute('id');
        $script = $script ?: $this->generateScripts();
        $script .= "
            $(document).on('change', '#{$tableId} .datatable-check-all', function(event){
                if($(this).prop('checked')){
                    $('#{$tableId} .datatable-row-checkbox').prop('checked',true).iCheck('update');
                }else{
                    $('#{$tableId} .datatable-row-checkbox').prop('checked',false).iCheck('update');
                }
            });
            
            $(document).on('change', '#{$tableId} .datatable-row-checkbox', function(event){
                var checkboxes = $('#{$tableId} .datatable-row-checkbox');
                
                if (checkboxes.length == checkboxes.filter(':checked').length) {
                    $('#{$tableId} .datatable-check-all').prop('checked', 'checked').iCheck('update');
                } else {
                    $('#{$tableId} .datatable-check-all').prop('checked', false).iCheck('update');
                }
            });
            ";
        return parent::scripts($script, $attributes);
    }

    public function addCheckbox(array $attributes = [], $position = false)
    {
        $dataTableId = array_pull($attributes, 'datatable_id');
        $title = '<input type="checkbox" class="datatable-check-all" id="' .
            $dataTableId . '_dataTablesCheckbox' . '"/>';
        $attributes = array_merge([
            'defaultContent' => '<input type="checkbox" class="datatable-row-checkbox" />',
            'title'          => $title,
            'data'           => 'checkbox',
            'name'           => 'checkbox',
            'orderable'      => false,
            'searchable'     => false,
            'exportable'     => false,
            'printable'      => true,
            'width'          => '10px',
        ], $attributes);
        $column = new Column($attributes);
        if ($position === true) {
            $this->collection->prepend($column);
        } elseif ($position === false || $position >= $this->collection->count()) {
            $this->collection->push($column);
        } else {
            $this->collection->splice($position, 0, [$column]);
        }
        return $this;
    }
}
